function fonction(ville) {
  //alert('yolo');
  var zone = document.querySelector('.pageZone');
  var phrase = "";

  switch (true) {
    case (ville == "Paris"):
      phrase = "Il fait toujours gris à Paris.";
      break;
    case (ville.length <= 5):
      phrase = "Il fera très beau à " + ville + ".";
      break;
    case (ville.length > 5 && ville.length <= 10):
      phrase = "Il fera super beau à " + ville + ".";
      break;

    case (ville.length > 10):
      phrase = "Il fera plutôt beau à " + ville + ".";
      break;

    default:
      phrase = "Comment ? " + ville + " ?";
      break;
  }
  return phrase;
}

module.exports = fonction;