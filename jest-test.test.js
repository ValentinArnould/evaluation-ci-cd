const fonction = require('./script');

describe('Testing météo', () => {
  var court = "Il fera très beau à ";
  var moyen = "Il fera super beau à ";
  var long = "Il fera plutôt beau à ";
  var Paris = "Il fait toujours gris à Paris.";
  var ville = "";

  test('essai pour Lyon', () => {
    ville = "Lyon";
    expect(fonction(ville) == (court + ville + "."));
  });

  test('essai pour Sallanches', () => {
    ville = "Sallanches";
    expect(fonction(ville) == (moyen + ville + "."));
  });

  test('essai pour Aoste', () => {
    ville = "Aoste";
    expect(fonction(ville) == (court + ville + "."));
  });

  test('essai pour Château-Roux', () => {
    ville = "Château-Roux";
    expect(fonction(ville) == (long + ville + "."));
  });

  test('essai pour Paris', () => {
    ville = "Paris";
    expect(fonction(ville) == (Paris));
  });
});


/*test('nom du test', () => {
  expect("fonction(valeurs)" == ("result"));
});*/