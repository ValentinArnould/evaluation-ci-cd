# Evaluation CI CD

Projet d'évaluation d'industrialisation et de CI/CD.

  - Ecriture d'une application en HTML classique (avec javascript)
  - Implémentation de tests unitaires (avec Jest via npm)
  - Prise en charge avec GitLab d'un CI/CD
  - Déploiement de l'application sur serveur par protocole ftp

# Programme

  - Page html avec un champ, permettant de trouver la météo d'une ville (c'est fou, il fait toujours beau)

# Tests unitaires

  - Installation et configuration de Jest avec npm
  - Intégration d'un fichier jest-test.test.js
  - Création des tests unitaires sur la fonction du script.js

# CI CD

  - Utilisation d'un dépôt GitLab
  - Ajout de la configuration git-ci.yml
  - Utilisation de gulp pour la compilation du css via npm
  - Configuration des variables sensibles dans GitLab (user,pass,host)

# Déploiement

  - Utilisation de ltfp pour le déploiement de l'application sur le serveur ftp

# Soucis
  - Perte de temps dans le développement de l'application
  - Perte de temps d'utilisation de Jest
  - Impossible de déployer sur le serveur ftp (error)
  - Sinon TU, CI et Packaging fonctionne
  - Réutilisation de l'ancien ftp

Pseudo GitLab : ValentinArnould
